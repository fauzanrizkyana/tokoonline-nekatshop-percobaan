<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @author Andi Siswanto <andisis92@gmail.com>
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "c8028b3d956495251b8d7dc1e3b8ad60";

/**
 * RajaOngkir account type: starter / basic / pro
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "starter";
