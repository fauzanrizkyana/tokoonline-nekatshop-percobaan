<div id="colorlib-shop" style="padding-bottom: 50px; padding-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-3">
                <div class="row">
                    <div class="col-md-7 tabulation">
                        <ul class="nav nav-tabs">
                            <li><a href="<?= base_url(); ?>auth">Login</a></li>
                            <li class="active"><a data-toggle="tab" href="#">Register</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="login" class="tab-pane fade in active">
                                <form action="<?= base_url('auth/register'); ?>" method="post" class="colorlib-form" style="background-color: #ffc107;">
                                    <h3 class="text-center">Register</h3>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="username">Nama</label>
                                                <input type="text" id="username" name="username" class="form-control form-control-sm" style="height: 30px;" placeholder="Nama" value="<?= set_value('username'); ?>">
                                                <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="tanggal_lahir">Tanggal Lahir (mm/dd/yyyy)</label>
                                                <div class="input-group date">
                                                    <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control form-control-sm" style="height: 40px;" placeholder="mm/dd/yyyy" value="<?= set_value('tanggal_lahir'); ?>">
                                                    <div class="input-group-addon">
                                                        <span class="icon icon-calendar"></span>
                                                    </div>
                                                </div>
                                                <?= form_error('tanggal_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="jenis_kelamin">Jenis kelamin</label>
                                                <div class="radio">
                                                    <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="1"> Pria</label>
                                                    <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="2"> Wanita</label>
                                                </div>
                                                <?= form_error('jenis_kelamin', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="email">Email</label>
                                                <input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 30px;" placeholder="Email" value="<?= set_value('email'); ?>">
                                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="no_telepon">No. Telepon</label>
                                                <input type="text" id="no_telepon" name="no_telepon" class="form-control form-control-sm" style="height: 30px;" placeholder="No. telepon" value="<?= set_value('no_telepon'); ?>">
                                                <?= form_error('no_telepon', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label for="password1">Password</label>
                                                        <input type="password" id="password1" name="password1" class="form-control form-control-sm" style="height: 30px;" placeholder="Password" value="<?= set_value('password1'); ?>">
                                                        <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="password2">Konfirmasi Password</label>
                                                        <input type="password" id="password2" name="password2" class="form-control form-control-sm" style="height: 30px;" placeholder="Konfirmasi Password" value="<?= set_value('password2'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="checkbox" id="accept" name="accept" value="accept">Saya telah menerima ketentuan yang berlaku
                                        </div>
                                        <?= form_error('accept', '<small class="text-danger pl-3">', '</small>'); ?>
                                        <div class="form-group text-center">
                                            <input type="submit" name="submit" value="Register" class="btn btn-secondary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>