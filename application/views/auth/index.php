        <div id="colorlib-shop" style="padding-bottom: 50px; padding-top: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-3">
                        <div class="row">
                            <div class="col-md-7 tabulation">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#login">Login</a></li>
                                    <li><a href="<?= base_url(); ?>auth/register">Register</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="login" class="tab-pane fade in active">
                                        <form action="<?= base_url(); ?>auth" method="post" class="colorlib-form" style="background-color: #ffc107;">
                                            <h3 class="text-center">Log in</h3>
                                            <?= $this->session->flashdata('message'); ?>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label for="email">Email</label>
                                                        <input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 30px;" placeholder="Email" value="<?= set_value('email'); ?>">
                                                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label for="password">Password</label>
                                                        <input type="password" id="password" name="password" class="form-control form-control-sm" style="height: 30px;" placeholder="Password">
                                                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="checkbox" id="remember" name="remember" value="remember">Remember Me
                                                </div>
                                                <div class="form-group text-center">
                                                    <input type="submit" name="submit" value="Login" class="btn btn-secondary">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>