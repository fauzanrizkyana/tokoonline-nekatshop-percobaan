<?= $this->session->flashdata('message'); ?>
<div class="colorlib-about" style="padding-top: 50px;">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-8 col-md-offset-2">
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="<?= base_url(); ?>assets/img2/<?= $user['image']; ?>" class="card-img">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h3 class="card-title">Profil</h3>
                                <p class="card-text">
                                    Username : <?= $user['username']; ?><br>
                                    Tanggal lahir : <?= date('d F Y', $user['tanggal_lahir']); ?><br>
                                    Jenis kelamin : <?= $user['jenis_kelamin']; ?><br>
                                    Email : <?= $user['email']; ?><br>
                                    No. telepon : <?= $user['no_telepon']; ?><br>
                                    <small class="text-muted">Akun dibuat sejak <?= date('d F Y H:i', $user['date_created']); ?></small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?= base_url(); ?>profile/edit" class="btn btn-primary">Edit profil</a>
                <a href="<?= base_url(); ?>profile/changeemailphone" class="btn btn-warning">Ganti email / no. telepon</a>
                <a href="<?= base_url(); ?>profile/changepassword" class="btn btn-danger">Ganti Password</a>
            </div>
        </div>
    </div>
</div>