<div class="colorlib-contact" style="padding-top: 50px;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="contact-wrap" style="background-color: #ffc107;">
                <h3>Ubah Password</h3>
                <form action="<?= base_url('profile/changepassword'); ?>" method="post">
                    <div class="form-group">
                        <label for="current_password">Password saat ini</label>
                        <input type="password" id="current_password" name="current_password" class="form-control form-control-sm" style="height: 30px;">
                        <?= form_error('current_password', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="new_password1">Password baru</label>
                        <input type="password" id="new_password1" name="new_password1" class="form-control form-control-sm" style="height: 30px;">
                        <?= form_error('new_password1', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="new_password2">Konfirmasi password</label>
                        <input type="password" id="new_password2" name="new_password2" class="form-control form-control-sm" style="height: 30px;">
                        <?= form_error('new_password2', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Ubah password" class="btn btn-primary" style="background-color: yellow; color: black;">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>