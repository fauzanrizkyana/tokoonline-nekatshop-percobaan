<div class="colorlib-contact" style="padding-top: 50px;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="contact-wrap" style="background-color: #ffc107;">
                <h3>Edit Profil</h3>
                <?= form_open_multipart('profile/edit'); ?>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 30px;" value="<?= $user['email']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username" class="form-control form-control-sm" style="height: 30px;" value="<?= $user['username']; ?>">
                    <?= form_error('username', '<small class="form-text text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="tanggal_lahir">Tanggal lahir (mm/dd/yyyy)</label>
                    <div class="input-group date">
                        <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control form-control-sm" style="height: 40px;" value="<?= date('m/d/Y', $user['tanggal_lahir']); ?>">
                        <div class="input-group-addon">
                            <span class="icon icon-calendar"></span>
                        </div>
                    </div>
                    <?= form_error('tanggal_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="jenis_kelamin">Jenis kelamin</label>
                    <div class="radio">
                        <?php foreach ($jenis_kelamin as $jk) : ?>
                            <?php if ($user['id_jenis_kelamin'] == $jk['id']) : ?>
                                <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="<?= $jk['id']; ?>" checked> <?= $jk['jenis_kelamin']; ?></label>
                            <?php else : ?>
                                <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="<?= $jk['id']; ?>"> <?= $jk['jenis_kelamin']; ?></label>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <?= form_error('jenis_kelamin', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="image">Gambar</label>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="<?= base_url(); ?>assets/img2/<?= $user['image']; ?>" class="img-thumbnail">
                                </div>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="image">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Edit" class="btn btn-primary" style="background-color: yellow; color: black;">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>