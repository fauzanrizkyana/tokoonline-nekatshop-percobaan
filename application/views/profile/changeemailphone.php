<div class="colorlib-contact" style="padding-top: 50px;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="contact-wrap" style="background-color: #ffc107;">
                <h3>Ubah Email atau No. Telepon</h3>
                <form action="<?= base_url(); ?>profile/changeemailphone" method="post">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 30px;" value="<?= $user['email']; ?>">
                        <?= form_error('email', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="no_telepon">No. Telepon</label>
                        <input type="text" id="no_telepon" name="no_telepon" class="form-control form-control-sm" style="height: 30px;" value="<?= $user['no_telepon']; ?>">
                        <?= form_error('no_telepon', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Ubah" class="btn btn-primary" style="background-color: yellow; color: black;">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>