<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $judul; ?> | Nekatshop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/magnific-popup.css">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/flexslider.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/owl.theme.default.min.css">

    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/bootstrap-datepicker.css">
    <!-- Flaticons  -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/fonts/flaticon/font/flaticon.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?= base_url(); ?>assets/store/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

    <div class="colorlib-loader"></div>

    <nav class="colorlib-nav navbar-default" role="navigation" style="height: 65px; width: 100%; background-color: white-smoke;">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2" style="position: absolute; top: 15px; left: 100px;">
                        <div id="colorlib-logo"><a href="<?= base_url(); ?>">Nekatshop</a></div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div id="page">