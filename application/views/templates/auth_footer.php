<footer id="colorlib-footer" role="contentinfo" style="background-color: whitesmoke;">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-6 colorlib-widget">
                <h4>Tentang Nekatshop</h4>
                <p>Nekatshop adalah sebuah situs ecommerce fashion pertama dan terbaik di kecamatan Katapang.</p>
                <p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-instagram"></i></a></li>
                        <li><a href="#"><i class="icon-globe"></i></a></li>
                    </ul>
                </p>
            </div>
            <div class="col-md-6">
                <h4>Informasi Kontak</h4>
                <ul class="colorlib-footer-links">
                    <li>Jalan Terusan Kopo KM 12 , Katapang Kab. Bandung 40921</li>
                    <li><a href="tel://62215436789">+ 62 21 543 6789</a></li>
                    <li><a href="mailto:info@nekatshop.com">info@nekatshop.com</a></li>
                    <li><a href="<?= base_url(); ?>">nekatshop.com</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copy">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>
                    <span class="block">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<?= date('Y'); ?> All rights reserved | This template is made with <i class="icon-heart2" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </span>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- jQuery -->
<script src="<?= base_url(); ?>assets/store/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?= base_url(); ?>assets/store/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url(); ?>assets/store/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>

<!-- Waypoints -->
<script src="<?= base_url(); ?>assets/store/js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="<?= base_url(); ?>assets/store/js/jquery.flexslider-min.js"></script>
<!-- Owl carousel -->
<script src="<?= base_url(); ?>assets/store/js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="<?= base_url(); ?>assets/store/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url(); ?>assets/store/js/magnific-popup-options.js"></script>
<!-- Date Picker -->
<script src="<?= base_url(); ?>assets/store/js/bootstrap-datepicker.js"></script>
<script>
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy'
    });
</script>
<!-- Stellar Parallax -->
<script src="<?= base_url(); ?>assets/store/js/jquery.stellar.min.js"></script>
<!-- Main -->
<script src="<?= base_url(); ?>assets/store/js/main.js"></script>
<script src="http://localhost/ci-adminlte/assets/AdminLTE/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url(); ?>assets/js/myscript.js"></script>
</body>

</html>