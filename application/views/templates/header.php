<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $judul; ?> | Nekatshop</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap-4.3.1-dist/css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/flexslider.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/owl.theme.default.min.css">

	<!-- Date Picker -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/bootstrap-datepicker.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/fonts/flaticon/font/flaticon.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/store/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?= base_url(); ?>assets/store/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div class="colorlib-loader"></div>

	<nav class="colorlib-nav navbar-default" role="navigation" style="position: fixed; z-index: 200; height: 65px; width: 100%; background-color: white-smoke;">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-2" style="position: absolute; top: 15px; left: 100px;">
						<div id="colorlib-logo"><a href="<?= base_url(); ?>">Nekatshop</a></div>
					</div>
					<div class="col-xs-12 text-right menu-1">
						<div style="position: absolute; top: -25px; left: 200px;">
							<form class="form-inline" role="search" action="<?= base_url() ?>search" method="post">
								<select class="custom-select mr-sm-2" name="jenis" style="background-color: orange; width: 65px;">
									<option value="Semua">Semua</option>
									<?php $jenis = $this->db->get_where('t_kategori', ['active' => 1])->result_array();
																			foreach ($jenis as $j) : ?>
										<option value="<?= $j['id']; ?>"><?= $j['kependekan']; ?></option>
									<?php endforeach; ?>
								</select>
								<div class="input-group input-group-sm">
									<input type="text" class="form-control" type="search" name="keyword" placeholder="Cari produk...." autocomplete="off" style="width: 150px;">
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit" style="color: black;">Cari</button>
									</span>
								</div>
							</form>
						</div>
						<div style="position: absolute; top: -30px; right: 0px;">
							<ul>
								<li <?= $this->uri->segment(1) == '' ? 'class="active"' : $this->uri->segment(1) == 'home' ? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>">Home</a></li>
								<li class="has-dropdown <?= $this->uri->segment(1) == 'shop' ? $this->uri->segment(2) != 'cart' ? $this->uri->segment(2) != 'checkout' ? 'active' : '' : '' : ''; ?>">
									<a href="#">Shop</a>
									<ul class="dropdown">
										<li><a href="<?= base_url(); ?>shop">Semua Produk</a></li>
										<?php $jenis = $this->db->get_where('t_kategori', ['active' => 1])->result_array();
																																								foreach ($jenis as $j) : ?>
											<li><a href="<?= base_url(); ?>shop?category=<?= $j['id'] ?>"><?= $j['jenis_produk'] ?></a></li>
										<?php endforeach; ?>
									</ul>
								</li>
								<li <?= $this->uri->segment(1) == 'blog' ? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>blog">Berita</a></li>
								<li <?= $this->uri->segment(1) == 'about' ? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>about">Tentang</a></li>
								<li <?= $this->uri->segment(1) == 'contact' ? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>contact">Kontak</a></li>
								<li <?= $this->uri->segment(2) == 'cart' ? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>shop/cart"><i class="icon-shopping-cart"></i> [<?= $this->cart->total_items(); ?>]</a></li>
								<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
									<li class="has-dropdown">
										<a href="#">
											<img src="<?= base_url(); ?>assets/img2/<?= $user['image']; ?>" class="rounded-circle" width="30" height="30" alt="">
											<?= word_limiter($user['username'], 2); ?>
										</a>
										<ul class="dropdown">
											<li><a href="<?= base_url(); ?>profile">Profil</a></li>
											<li><a href="<?= base_url(); ?>shop/wishlist">Wishlist</a></li>
											<li><a href="<?= base_url(); ?>auth/logout">Logout</a></li>
										</ul>
									</li>
								<?php else : ?>
									<li><a href="<?= base_url(); ?>auth">Login</a></li>
									<li><a href="<?= base_url(); ?>auth/register">Reg</a></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<div id="page" style="padding-top: 65px">