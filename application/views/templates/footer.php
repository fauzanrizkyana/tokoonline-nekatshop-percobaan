<div id="colorlib-subscribe">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-6 text-center">
					<h2><i class="icon-paperplane"></i>Daftarlah untuk mendapatkan informasi terkini</h2>
				</div>
				<div class="col-md-6">
					<form class="form-inline qbstp-header-subscribe">
						<div class="row">
							<div class="col-md-12 col-md-offset-0">
								<div class="form-group">
									<input type="text" class="form-control" style="width: 300px;" id="email" placeholder="Email">
									<button type="submit" class="btn btn-primary">Subscribe</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<footer id="colorlib-footer" role="contentinfo" style="background-color: whitesmoke;">
	<div class="container">
		<div class="row row-pb-md">
			<div class="col-md-3 colorlib-widget">
				<h4>Tentang Nekatshop</h4>
				<p>Nekatshop adalah sebuah situs ecommerce fashion pertama dan terbaik di kecamatan Katapang.</p>
				<p>
					<ul class="colorlib-social-icons">
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-instagram"></i></a></li>
						<li><a href="#"><i class="icon-globe"></i></a></li>
					</ul>
				</p>
			</div>
			<div class="col-md-2 colorlib-widget">
				<h4>Layanan</h4>
				<p>
					<ul class="colorlib-footer-links">
						<li><a href="<?= base_url(); ?>contact">Kontak</a></li>
						<li><a href="#">Pengembalian Kembali</a></li>
						<li><a href="#">Special</a></li>
						<li><a href="#">Customer Services</a></li>
					</ul>
				</p>
			</div>
			<div class="col-md-2 colorlib-widget">
				<h4>Informasi</h4>
				<p>
					<ul class="colorlib-footer-links">
						<li><a href="<?= base_url(); ?>about">Tentang Kami</a></li>
						<li><a href="#">Delivery Information</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Order Tracking</a></li>
					</ul>
				</p>
			</div>

			<div class="col-md-2">
				<h4>Blog</h4>
				<ul class="colorlib-footer-links">
					<li><a href="<?= base_url(); ?>blog">Berita</a></li>
					<li><a href="#">Press</a></li>
					<li><a href="#">Exhibitions</a></li>
				</ul>
			</div>

			<div class="col-md-3">
				<h4>Informasi Kontak</h4>
				<ul class="colorlib-footer-links">
					<li>Jalan Terusan Kopo KM 12 , <br> Katapang Kab. Bandung 40921</li>
					<li><a href="tel://62215436789">+ 62 21 543 6789</a></li>
					<li><a href="mailto:info@nekatshop.com">info@nekatshop.com</a></li>
					<li><a href="<?= base_url(); ?>">nekatshop.com</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="copy">
		<div class="row">
			<div class="col-md-12 text-center">
				<p>
					<span class="block">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<?= date('Y'); ?> All rights reserved | This template is made with <i class="icon-heart2" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</span>
				</p>
			</div>
		</div>
	</div>
</footer>
</div>

<div class="gototop js-top">
	<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- jQuery -->
<script src="<?= base_url(); ?>assets/store/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?= base_url(); ?>assets/store/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url(); ?>assets/store/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>

<!-- Waypoints -->
<script src="<?= base_url(); ?>assets/store/js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="<?= base_url(); ?>assets/store/js/jquery.flexslider-min.js"></script>
<!-- Owl carousel -->
<script src="<?= base_url(); ?>assets/store/js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="<?= base_url(); ?>assets/store/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url(); ?>assets/store/js/magnific-popup-options.js"></script>
<!-- Date Picker -->
<script src="<?= base_url(); ?>assets/store/js/bootstrap-datepicker.js"></script>
<script>
	$('.datepicker').datepicker({
		format: 'mm/dd/yyyy'
	});
</script>
<!-- Stellar Parallax -->
<script src="<?= base_url(); ?>assets/store/js/jquery.stellar.min.js"></script>
<!-- Main -->
<script src="<?= base_url(); ?>assets/store/js/main.js"></script>
<script>
	function tambahjumlah() {
		var quantity = parseInt(document.getElementById("qty").value);
		if (quantity < 100) {
			document.getElementById("qty").value = quantity += 1;
		}
	}

	function kurangjumlah() {
		var quantity = parseInt(document.getElementById("qty").value);
		if (quantity > 1) {
			document.getElementById("qty").value = quantity -= 1;
		}
	}
</script>
<script src="http://localhost/ci-adminlte/assets/AdminLTE/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url(); ?>assets/js/myscript.js"></script>
<script src="<?= base_url(); ?>assets/js/rajaongkirscript.js"></script>
<script>
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	}

	function tambah() {
		var shipping = parseInt(document.getElementById("list_kurir_div").value);
		var subtotal = parseInt(document.getElementById("subtotal").value);
		var total = subtotal + shipping;
		document.getElementById("shipping").innerHTML = 'Rp. ' + formatNumber(shipping);
		document.getElementById("grandtotalview").innerHTML = 'Rp. ' + formatNumber(total);
		document.getElementById("grandtotal").value = total;
	}
</script>
<script>
	$('.custom-file-input').on('change', function() {
		let fileName = $(this).val().split('\\').pop();
		$(this).next('.custom-file-label').addClass("selected").html(fileName);
	});
</script>
<script src="<?= base_url(); ?>assets/js/payment.js"></script>
</body>

</html>