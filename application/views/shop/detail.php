		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
					<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h1><?= $judul2; ?></h1>
										<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><a href="<?= base_url(); ?>shop">Shop</a></span> <span><?= $judul2; ?></span></h2>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-md-10 col-md-offset-1">
						<div class="product-detail-wrap">
							<div class="row">
								<div class="col-md-5">
									<div class="product-entry">
										<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $produk['gambar_produk']; ?>);">
											<?php if ($produk['diskon'] > 0) : ?>
												<p class="tag"><span class="sale"><?= $produk['diskon']; ?>%</span></p>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="desc">
										<form action="<?= base_url(); ?>shop/tambah" method="post">
											<h3><?= $produk['nama_produk']; ?></h3>
											<input type="hidden" name="id" value="<?= $produk['id_produk']; ?>">
											<input type="hidden" name="nama" value="<?= $produk['nama_produk']; ?>">
											<?php if ($produk['diskon'] > 0) :
												$sale = $produk['harga_produk'] - ($produk['harga_produk'] * $produk['diskon'] / 100); ?>
												<p class="price">
													<small><del>Rp. <?= number_format($produk['harga_produk'], '0', ',', '.'); ?></del></small><br>
													<span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span>
												</p>
											<?php else :
												$sale = $produk['harga_produk']; ?>
												<p class="price">
													<span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span>
												</p>
											<?php endif; ?>
											<input type="hidden" name="harga" value="<?= $sale; ?>">
											<p><?= $produk['deskripsi_produk']; ?></p>
											<input type="hidden" name="gambar" value="<?= $produk['gambar_produk']; ?>">
											<?php if ($produk['stok_produk'] != 0) : ?>
												<p>Tersisa : <?= $produk['stok_produk']; ?> buah</p>
												<div class="size-wrap">
													<p class="size-desc">
														<?php if ($produk['jenis_produk'] != 'Alas Kaki') : ?>
															<div class="form-group">
																<label for="ukuran">Ukuran: </label>
																<select class="form-control" style="width: 150px;" id="ukuran" name="ukuran">
																	<option value="XS">XS</option>
																	<option value="S">S</option>
																	<option value="M">M</option>
																	<option value="L">L</option>
																	<option value="XL">XL</option>
																	<option value="XXL">XXL</option>
																</select>
															</div>
														<?php else : ?>
															<div class="form-group">
																<label for="ukuran">Ukuran:</label>
																<input type="number" id="ukuran" name="ukuran" class="form-control input-number" style="width: 150px;" value="35" min="35" max="48" step="0.5">
															</div>
														<?php endif; ?>
													</p>
												</div>
												<div class="row row-pb-sm">
													<div class="col">
														<div class="form-group">
															<div class="row">
																<div class="col-md-1">
																	<span class="input-group-btn">
																		<button type="button" class="quantity-left-minus btn" data-type="minus" data-field="" onclick="kurangjumlah()">
																			<i class="icon-minus2"></i>
																		</button>
																	</span>
																</div>
																<input type="text" id="qty" name="qty" class="form-control" style="width: 100px;" value="1" min="1" max="100" readonly>
																<span class="input-group-btn">
																	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="" onclick="tambahjumlah()">
																		<i class="icon-plus2"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>
												</div>
												<button class="btn btn-primary btn-addtocart" type="submit">Tambah ke keranjang</button>
											<?php else : ?>
												<p><b style="color : red;">Stok habis</b></p>
												<p><b style="color : red;">Semoga beruntung lain kali</b></p>
											<?php endif; ?>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Produk Lain</span></h2>
					</div>
				</div>
				<div class="row">
					<?php foreach ($recent_produk as $rp) : ?>
						<div class="col-md-3 text-center">
							<div class="product-entry">
								<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $rp['gambar_produk']; ?>);">
									<?php if ($rp['diskon'] > 0) : ?>
										<p class="tag"><span class="sale"><?= $rp['diskon']; ?>%</span></p>
									<?php endif; ?>
									<div class="cart">
										<p>
											<?php if ($rp['stok_produk'] != 0) : ?>
												<span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $rp['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
											<?php endif; ?>
											<span><a href="<?= base_url(); ?>shop/detail/<?= $rp['id_produk']; ?>"><i class="icon-eye"></i></a></span>
											<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
												<?php $cek_wishlist = $this->db->get_where('t_wishlist', ['email' => $this->session->userdata('email'), 'id_produk' => $rp['id_produk']])->row_array();
														if ($cek_wishlist == null) : ?>
													<span><a href="<?= base_url(); ?>shop/addtowishlist/<?= $rp['id_produk']; ?>"><i class="icon-star"></i></a></span>
												<?php else : ?>
													<span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $cek_wishlist['id']; ?>"><i class="icon-star-full"></i></a></span>
												<?php endif; ?>
											<?php endif; ?>
										</p>
									</div>
								</div>
								<div class="desc">
									<h3><a href="<?= base_url(); ?>shop/detail/<?= $rp['id_produk']; ?>"><?= $rp['nama_produk']; ?></a></h3>
									<?php if ($rp['diskon'] > 0) :
											$sale = $rp['harga_produk'] - ($rp['harga_produk'] * $rp['diskon'] / 100); ?>
										<p class="price"><span class="sale">Rp. <?= number_format($rp['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
									<?php else : ?>
										<p class="price"><span>Rp. <?= number_format($rp['harga_produk'], '0', ',', '.'); ?></span></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>