<aside id="colorlib-hero" class="breadcrumbs">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                            <div class="slider-text-inner text-center">
                                <h1>Checkout</h1>
                                <h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><a href="<?= base_url(); ?>shop">Shop</a></span> <span>Checkout</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>
<div class="colorlib-shop">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-10 col-md-offset-1">
                <div class="process-wrap">
                    <div class="process text-center active">
                        <p><span>01</span></p>
                        <h3>Keranjang</h3>
                    </div>
                    <div class="process text-center active">
                        <p><span>02</span></p>
                        <h3>Checkout</h3>
                    </div>
                    <div class="process text-center">
                        <p><span>03</span></p>
                        <h3>Selesai</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?= $this->session->flashdata('message'); ?>
            <div class="col-md-6">
                <form action="<?= base_url() ?>shop/auth" method="post">
                    <div class="colorlib-form" style="background-color: #ffc107;">
                        <h2>Register</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="username">Nama</label>
                                    <input type="text" id="username" name="username" class="form-control form-control-sm" style="height: 36px;" placeholder="Username" value="<?= set_value('username'); ?>">
                                    <?= form_error('username', '<small class="form-text text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir (mm/dd/yyyy)</label>
                                    <div class="input-group date">
                                        <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control form-control-sm" style="height: 40px;" placeholder="mm/dd/yyyy" value="<?= set_value('tanggal_lahir'); ?>">
                                        <div class="input-group-addon">
                                            <span class="icon icon-calendar"></span>
                                        </div>
                                    </div>
                                    <?= form_error('tanggal_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis kelamin</label>
                                    <div class="radio">
                                        <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="1"> Pria</label>
                                        <label for="jenis_kelamin"><input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="2"> Wanita</label>
                                    </div>
                                    <?= form_error('jenis_kelamin', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 36px;" placeholder="Email" value="<?= set_value('email'); ?>">
                                    <?= form_error('email', '<small class="form-text text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="no_telepon">No. Telepon</label>
                                    <input type="text" id="no_telepon" name="no_telepon" class="form-control form-control-sm" style="height: 36px;" placeholder="No. Telepon" value="<?= set_value('no_telepon'); ?>">
                                    <?= form_error('no_telepon', '<small class="form-text text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="password1">Password</label>
                                            <input type="password" id="password1" name="password1" class="form-control form-control-sm" style="height: 30px;" placeholder="Password" value="<?= set_value('password1'); ?>">
                                            <?= form_error('password1', '<small class="form-text text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="password2">Konfirmasi Password</label>
                                            <input type="password" id="password2" name="password2" class="form-control form-control-sm" style="height: 30px;" placeholder="Konfirmasi Password" value="<?= set_value('password2'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" id="accept" name="accept" value="accept">Saya telah menerima ketentuan yang berlaku
                            </div>
                            <div class=" col-md-12">
                                <p><input type="submit" name="daftar" class="btn btn-secondary" value="Daftar"></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <form action="<?= base_url() ?>shop/auth" method="post">
                    <div class="colorlib-form" style="background-color: #ffc107;">
                        <h2>Punya akun? Login sekarang!</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email_login">Email</label>
                                    <input type="text" id="email_login" name="email_login" class="form-control form-control-sm" style="height: 36px;" placeholder="Email" value="<?= set_value('email_login'); ?>">
                                    <?= form_error('email_login', '<small class="form-text text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password_login">Password</label>
                                    <input type="password" id="password_login" name="password_login" class="form-control form-control-sm" style="height: 36px;" placeholder="Password" value="<?= set_value('password_login'); ?>">
                                    <?= form_error('password_login', '<small class="form-text text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" id="remember" name="remember" value="remember">Remember Me
                            </div>
                            <div class="col-md-12">
                                <p><input type="submit" name="login" class="btn btn-secondary" value="Login"></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>