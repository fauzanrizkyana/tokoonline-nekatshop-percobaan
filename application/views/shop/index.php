<aside id="colorlib-hero" class="breadcrumbs">
	<div class="flexslider">
		<ul class="slides">
			<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
				<div class="overlay"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
							<div class="slider-text-inner text-center">
								<h1>Produk</h1>
								<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><?= $judul; ?></span></h2>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>
<div class="colorlib-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-push-2">
				<div class="row row-pb-lg">
					<?php if ($produk == null) :  ?>
						<div class="col-md-12 text-center">
							<h2><span>Daftar produk kosong</span></h2>
						</div>
					<?php else : ?>
						<?php foreach ($produk as $prdk) : ?>
							<div class="col-md-4 text-center">
								<div class="product-entry">
									<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $prdk['gambar_produk']; ?>);">
										<?php if ($prdk['diskon'] > 0) : ?>
											<p class="tag"><span class="sale"><?= $prdk['diskon']; ?>%</span></p>
										<?php endif; ?>
										<div class="cart">
											<p>
												<?php if ($prdk['stok_produk'] != 0) : ?>
													<span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $prdk['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
												<?php endif; ?>
												<span><a href="<?= base_url(); ?>shop/detail/<?= $prdk['id_produk']; ?>"><i class="icon-eye"></i></a></span>
												<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
													<?php $cek_wishlist = $this->db->get_where('t_wishlist', ['email' => $this->session->userdata('email'), 'id_produk' => $prdk['id_produk']])->row_array();
																if ($cek_wishlist == null) : ?>
														<span><a href="<?= base_url(); ?>shop/addtowishlist/<?= $prdk['id_produk']; ?>"><i class="icon-star"></i></a></span>
													<?php else : ?>
														<span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $cek_wishlist['id']; ?>"><i class="icon-star-full"></i></a></span>
													<?php endif; ?>
												<?php endif; ?>
											</p>
										</div>
									</div>
									<div class="desc">
										<h3><a href="<?= base_url(); ?>shop/detail/<?= $prdk['id_produk']; ?>"><?= $prdk['nama_produk']; ?></a></h3>
										<?php if ($prdk['diskon'] > 0) :
													$sale = $prdk['harga_produk'] - ($prdk['harga_produk'] * $prdk['diskon'] / 100); ?>
											<p class="price"><span class="sale">Rp. <?= number_format($prdk['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
										<?php else : ?>
											<p class="price"><span>Rp. <?= number_format($prdk['harga_produk'], '0', ',', '.'); ?></span></p>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?= $pagination; ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-md-pull-10">
				<div class="sidebar">
					<div class="side">
						<h2>Kategori</h2>
						<ul>
							<li><a href="<?= base_url(); ?>shop">Semua</a></li>
							<?php $jenis = $this->db->get_where('t_kategori', ['active' => 1])->result_array();
							foreach ($jenis as $j) : ?>
								<li><a href="<?= base_url(); ?>shop?category=<?= $j['id'] ?>"><?= $j['jenis_produk'] ?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>