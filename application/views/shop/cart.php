        <aside id="colorlib-hero" class="breadcrumbs">
        	<div class="flexslider">
        		<ul class="slides">
        			<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
        				<div class="overlay"></div>
        				<div class="container-fluid">
        					<div class="row">
        						<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
        							<div class="slider-text-inner text-center">
        								<h1>Keranjang</h1>
        								<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><a href="<?= base_url(); ?>shop">Shop</a></span> <span>Keranjang</span></h2>
        							</div>
        						</div>
        					</div>
        				</div>
        			</li>
        		</ul>
        	</div>
        </aside>
        <div class="colorlib-shop">
        	<div class="container">
        		<?php if ($cart != null) : ?>
        			<div class="row row-pb-md">
        				<div class="col-md-10 col-md-offset-1">
        					<div class="process-wrap">
        						<div class="process text-center active">
        							<p><span>01</span></p>
        							<h3>Keranjang</h3>
        						</div>
        						<div class="process text-center">
        							<p><span>02</span></p>
        							<h3>Checkout</h3>
        						</div>
        						<div class="process text-center">
        							<p><span>03</span></p>
        							<h3>Selesai</h3>
        						</div>
        					</div>
        				</div>
        			</div>
        			<form action="<?php echo base_url() ?>shop/ubah" method="post" enctype="multipart/form-data">
        				<div class="row row-pb-md">
        					<div class="col-md-10 col-md-offset-1">
        						<div class="product-name">
        							<div class="one-forth text-center">
        								<span>Nama Produk</span>
        							</div>
        							<div class="one-eight text-center">
        								<span>Harga</span>
        							</div>
        							<div class="one-eight text-center">
        								<span>Banyak Barang</span>
        							</div>
        							<div class="one-eight text-center">
        								<span>Total</span>
        							</div>
        							<div class="one-eight text-center">
        								<span>Hapus</span>
        							</div>
        						</div>
        						<?php
																																$grand_total = 0;
																																$i = 1;
																																foreach ($cart as $item) :
																																	$grand_total = $grand_total + $item['subtotal'];
																																	$stok = $this->db->get_where('t_produk', ['id_produk' => $item['id']])->row_array();
								?>
        							<div class="product-cart">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][id]" value="<?= $item['id']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][rowid]" value="<?= $item['rowid']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][name]" value="<?= $item['name']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][price]" value="<?= $item['price']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][gambar]" value="<?= $item['gambar']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][ukuran]" value="<?= $item['ukuran']; ?>">
        								<input type="hidden" name="cart[<?= $item['id']; ?>][qtylama]" value="<?= $item['qty']; ?>">
        								<div class="one-forth">
        									<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $item['gambar']; ?>);">
        									</div>
        									<div class="display-tc">
        										<h3><?= $item['name']; ?> (Stok : <?= $stok['stok_produk']; ?>)</h3>
        									</div>
        								</div>
        								<div class="one-eight text-center">
        									<div class="display-tc">
        										<span class="price">Rp. <?= number_format($item['price'], '0', ',', '.'); ?></span>
        									</div>
        								</div>
        								<div class="one-eight text-center">
        									<div class="display-tc">
        										<input type="text" id="qty" name="cart[<?= $item['id']; ?>][qty]" class="form-control input-number text-center" value="<?= $item['qty']; ?>" min="1" max="100">
        									</div>
        								</div>
        								<div class="one-eight text-center">
        									<div class="display-tc">
        										<span class="price">Rp. <?= number_format($item['subtotal'], '0', ',', '.'); ?></span>
        									</div>
        								</div>
        								<div class="one-eight text-center">
        									<div class="display-tc">
        										<a href="<?= base_url(); ?>shop/hapus/<?= $item['rowid']; ?>" class="closed tombol-hapus"></a>
        									</div>
        								</div>
        							</div>
        						<?php endforeach; ?>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-md-10 col-md-offset-1">
        						<div class="total-wrap">
        							<div class="row">
        								<div class="col-md-8">
        									<?php if ($this->cart->contents()) : ?>
        										<a href="<?= base_url(); ?>shop/hapus/all" class="btn btn-danger tombol-hapus-semua">Hapus Keranjang</a>
        										<input type="submit" value="Update" class="btn btn-warning">
        										<a href="<?= base_url(); ?>shop/checkout" class="btn btn-success">Checkout</a>
        									<?php endif; ?>
        								</div>
        								<div class="col-md-4 text-center">
        									<div class="total">
        										<div class="grand-total">
        											<p><span><strong>Total:</strong></span> <span>Rp. <?= number_format($grand_total, '0', ',', '.'); ?></span></p>
        										</div>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        				</div>
        			</form>
        		<?php else : ?>
        			<div class="row row-pb-md">
        				<div class="col-md-6 col-md-offset-3 text-center">
        					<h2><span>Keranjang anda kosong</span></h2>
        					<p>Silakan belanja kembali</p>
        				</div>
        			</div>
        		<?php endif; ?>
        	</div>
        </div>