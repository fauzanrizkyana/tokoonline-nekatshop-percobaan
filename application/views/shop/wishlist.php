<aside id="colorlib-hero" class="breadcrumbs">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                            <div class="slider-text-inner text-center">
                                <h1>Wishlist</h1>
                                <h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span>Wishlist</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>
<div class="colorlib-shop">
    <div class="container">
        <div class="row row-pb-md">
            <?php if ($produk == null) : ?>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2><span>Daftar wishlist kosong</span></h2>
                </div>
            <?php else : ?>
                <?php foreach ($produk as $p) : ?>
                    <div class="col-md-3 text-center">
                        <div class="product-entry">
                            <div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $p['gambar_produk']; ?>);">
                                <?php if ($p['diskon'] > 0) : ?>
                                    <p class="tag"><span class="sale"><?= $p['diskon']; ?>%</span></p>
                                <?php endif; ?>
                                <div class="cart">
                                    <p>
                                        <?php if ($p['stok_produk'] != 0) : ?>
                                            <span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $p['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
                                        <?php endif; ?>
                                        <span><a href="<?= base_url(); ?>shop/detail/<?= $p['id_produk']; ?>"><i class="icon-eye"></i></a></span>
                                        <span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $p['id']; ?>"><i class="icon-star-full"></i></a></span>
                                    </p>
                                </div>
                            </div>
                            <div class="desc">
                                <h3><a href="<?= base_url(); ?>shop/detail/<?= $p['id_produk']; ?>"><?= $p['nama_produk']; ?></a></h3>
                                <?php if ($p['diskon'] > 0) :
                                                                                                                                $sale = $p['harga_produk'] - ($p['harga_produk'] * $p['diskon'] / 100); ?>
                                    <p class="price"><span class="sale">Rp. <?= number_format($p['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
                                <?php else : ?>
                                    <p class="price"><span>Rp. <?= number_format($p['harga_produk'], '0', ',', '.'); ?></span></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>