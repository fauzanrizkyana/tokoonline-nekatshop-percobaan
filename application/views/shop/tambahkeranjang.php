        <div class="colorlib-shop" style="padding-top: 100px;">
        	<div class="container">
        		<div class="row row-pb-lg">
        			<div class="col-md-10 col-md-offset-1">
        				<div class="product-detail-wrap">
        					<div class="row">
        						<div class="col-md-5">
        							<div class="product-entry">
        								<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $produk['gambar_produk']; ?>);">
        									<?php if ($produk['diskon'] > 0) : ?>
        										<p class="tag"><span class="sale"><?= $produk['diskon']; ?>%</span></p>
        									<?php endif; ?>
        								</div>
        							</div>
        						</div>
        						<div class="col-md-7">
        							<div class="desc">
        								<form action="<?= base_url(); ?>shop/tambah" method="post">
        									<h3><?= $produk['nama_produk']; ?></h3>
        									<input type="hidden" name="id" value="<?= $produk['id_produk']; ?>">
        									<input type="hidden" name="nama" value="<?= $produk['nama_produk']; ?>">
        									<?php if ($produk['diskon'] > 0) :
												$sale = $produk['harga_produk'] - ($produk['harga_produk'] * $produk['diskon'] / 100); ?>
        										<p class="price">
        											<small><del>Rp. <?= number_format($produk['harga_produk'], '0', ',', '.'); ?></del></small><br>
        											<span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span>
        										</p>
        										<input type="hidden" name="harga" value="<?= $sale; ?>">
        									<?php else : ?>
        										<p class="price">
        											<span>Rp. <?= number_format($produk['harga_produk'], '0', ',', '.'); ?></span>
        										</p>
        										<input type="hidden" name="harga" value="<?= $produk['harga_produk']; ?>">
        									<?php endif; ?>
        									<input type="hidden" name="gambar" value="<?= $produk['gambar_produk']; ?>">
        									<p>Tersisa : <?= $produk['stok_produk']; ?> buah</p>
        									<div class="size-wrap">
        										<p class="size-desc">
        											<?php if ($produk['jenis_produk'] != 'Alas Kaki') : ?>
        												<div class="form-group">
        													<label for="ukuran">Ukuran: </label>
        													<select class="form-control" style="width: 150px;" id="ukuran" name="ukuran">
        														<option value="XS">XS</option>
        														<option value="S">S</option>
        														<option value="M">M</option>
        														<option value="L">L</option>
        														<option value="XL">XL</option>
        														<option value="XXL">XXL</option>
        													</select>
        												</div>
        											<?php else : ?>
        												<div class="form-group">
        													<label for="ukuran">Ukuran:</label>
        													<input type="number" id="ukuran" name="ukuran" class="form-control input-number" style="width: 150px;" value="35" min="35" max="48" step="0.5">
        												</div>
        											<?php endif; ?>
        										</p>
        									</div>
        									<div class="row row-pb-sm">
        										<div class="col">
        											<div class="form-group">
        												<div class="row">
        													<div class="col-md-1">
        														<span class="input-group-btn">
        															<button type="button" class="quantity-left-minus btn" data-type="minus" data-field="" onclick="kurangjumlah()">
        																<i class="icon-minus2"></i>
        															</button>
        														</span>
        													</div>
        													<input type="text" id="qty" name="qty" class="form-control" style="width: 100px;" value="1" min="1" max="100" readonly>
        													<span class="input-group-btn">
        														<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="" onclick="tambahjumlah()">
        															<i class="icon-plus2"></i>
        														</button>
        													</span>
        												</div>
        											</div>
        										</div>
        									</div>
        									<button class="btn btn-primary btn-addtocart" type="submit">Tambah ke keranjang</button>
        								</form>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>