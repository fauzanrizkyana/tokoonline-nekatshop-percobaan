<aside id="colorlib-hero" class="breadcrumbs">
	<div class="flexslider">
		<ul class="slides">
			<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
				<div class="overlay"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
							<div class="slider-text-inner text-center">
								<h1>Checkout</h1>
								<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><a href="<?= base_url(); ?>shop">Shop</a></span> <span>Checkout</span></h2>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>
<div class="colorlib-shop">
	<div class="container">
		<div class="row row-pb-md">
			<div class="col-md-10 col-md-offset-1">
				<div class="process-wrap">
					<div class="process text-center active">
						<p><span>01</span></p>
						<h3>Keranjang</h3>
					</div>
					<div class="process text-center active">
						<p><span>02</span></p>
						<h3>Checkout</h3>
					</div>
					<div class="process text-center">
						<p><span>03</span></p>
						<h3>Selesai</h3>
					</div>
				</div>
			</div>
		</div>
		<form action="#" method="post">
			<div class="row">
				<div class="col-md-8">
					<div class="colorlib-form" style="background-color: #ffc107;">
						<h2>Checkout</h2>
						<div class="row">
							<input type="hidden" name="kode_pembayaran" value="<?= $kodePembayaran; ?>">
							<div class="col-md-12">
								<div class="form-group">
									<label for="kode_pembayaran">Kode Order</label>
									<input type="text" id="kode_pembayaran" class="form-control form-control-sm" style="height: 36px;" value="<?= $kodePembayaran; ?>" readonly>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="alamat">Alamat</label>
									<input type="text" id="alamat" class="form-control form-control-sm" style="height: 36px;" placeholder="Alamat">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="list_provinsi">Provinsi</label>
									<div class="form-field">
										<i class="icon icon-arrow-down3"></i>
										<select class="form-control form-control-sm" style="height: 36px;" id="list_provinsi">
											<option>- Pilih Provinsi -</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label for="list_kotakab">Kota/Kabupaten</label>
									<div class="form-field">
										<i class="icon icon-arrow-down3"></i>
										<select class="form-control form-control-sm" style="height: 36px;" name="kota_tujuan" id="list_kotakab">
											<option>- Pilih Kota / Kabupaten -</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<label for="kodepos">Kode Pos</label>
									<div id="kode_pos">
										<input type="text" class="form-control form-control-sm" style="height: 36px;" placeholder="Kode Pos">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="list_kurir">Kurir</label>
									<div class="form-field">
										<i class="icon icon-arrow-down3"></i>
										<select name="kurir_pengiriman" id="list_kurir" class="form-control form-control-sm" style="height: 36px;">
											<option>- Pilih Kurir -</option>
											<option value="jne">JNE</option>
											<option value="tiki">TIKI</option>
											<option value="pos">POS Indonesia</option>
										</select>
										<input type="hidden" name="berat_total" id="berat_total" value="<?= $this->cart->total_items() * 100; ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="list_kurir_div">Pilih paket</label>
									<div class="form-field">
										<i class="icon icon-arrow-down3"></i>
										<select class="form-control form-control-sm" style="height: 36px;" name="paket" id="list_kurir_div" onchange="tambah()">
											<option>- Pilih Paket -</option>
										</select>
									</div>
								</div>
								<div id="jenis_paket">
									<input type='hidden' name='nama_paket' value=''>
									<input type='hidden' name='jenis_paket' value=''>
									<input type='hidden' name='lama_pengiriman' value=''>
								</div>
								<div class="form-group">
									<label for="metode_pembayaran">Metode Pembayaran</label>
									<div class="col-md-12">
										<div class="radio">
											<label for="metode_pembayaran"><input type="radio" id="metode_pembayaran" name="metode_pembayaran" value="tunai" onchange="tunai()">Tunai</label>
											<label for="metode_pembayaran"><input type="radio" id="metode_pembayaran" name="metode_pembayaran" value="kartu kredit" onchange="kartuKredit()">Kartu Kredit</label>
										</div>
									</div>
								</div>
								<div id="form-metode"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cart-detail">
						<h2>Rincian</h2>
						<ul>
							<?php foreach ($produk as $p) : ?>
								<li><span><?= $p['qty'] . ' x ' . character_limiter($p['name'], 15, ''); ?></span> <span>Rp. <?= number_format($p['subtotal'], '0', ',', '.'); ?></span></li>
							<?php endforeach; ?>
							<li>
								<span>Subtotal</span> <span>Rp. <?= number_format($total, '0', ',', '.'); ?></b></span>
								<input type="hidden" name="subtotal" id="subtotal" value="<?= $total; ?>">
							</li>
							<li><span>Biaya Pengiriman</span> <span id="shipping">Rp. 0</span></li>
							<li>
								<span>Total</span> <span id="grandtotalview">Rp. <?= number_format($total, '0', ',', '.'); ?></span>
								<input type="hidden" name="grandtotal" id="grandtotal">
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12">
					<p><input type="submit" name="submit" class="btn btn-primary" value="Order"></p>
				</div>
			</div>
		</form>
	</div>
</div>