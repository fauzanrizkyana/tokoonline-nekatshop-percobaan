<aside id="colorlib-hero" class="breadcrumbs">
	<div class="flexslider">
		<ul class="slides">
			<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
				<div class="overlay"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
							<div class="slider-text-inner text-center">
								<h1><?= $judul; ?></h1>
								<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><?= $judul; ?></span></h2>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div id="colorlib-about">
	<div class="container">
		<div class="row">
			<div class="about-flex">
				<div class="col">
					<h1 class="text-center"><?= $judul; ?></h1>
					<div class="row">
						<?php foreach ($about as $a) : ?>
							<div class="col-md-12">
								<div class="row row-pb-sm">
									<div class="col">
										<img class="rounded mx-auto d-block" width="400px" src="http://localhost/ci-adminlte/assets/img4/<?= $a->gambar_about; ?>" alt="">
									</div>
									<div class="text-center">
										<?= $a->isi_about; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>