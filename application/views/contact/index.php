		<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
					<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h1><?= $judul; ?></h1>
										<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><?= $judul; ?></span></h2>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>
		<div id="colorlib-contact">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="contact-wrap" style="background-color: #ffc107;">
							<h3>Feedback</h3>
							<form action="" method="post">
								<div class="form-group">
									<label for="nama">Nama</label>
									<input type="text" id="nama" name="nama" class="form-control form-control-sm" style="height: 30px;" placeholder="Nama" value="<?= $user['username'] ? $user['username'] : set_value('nama'); ?>">
									<?= form_error('nama', '<small class="form-text text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="text" id="email" name="email" class="form-control form-control-sm" style="height: 30px;" placeholder="Email" value="<?= $user['email'] ? $user['email'] : set_value('email'); ?>">
									<?= form_error('email', '<small class="form-text text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<label for="subjek">Subjek</label>
									<input type="text" id="subjek" name="subjek" class="form-control form-control-sm" style="height: 30px;" placeholder="Subjek" value="<?= set_value('subjek'); ?>">
									<?= form_error('subjek', '<small class="form-text text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<label for="pesan">Pesan</label>
									<textarea name="pesan" id="pesan" cols="30" rows="5" class="form-control form-control-sm" placeholder="Katakan sesuatu tentang kami"><?= set_value('pesan'); ?></textarea>
									<?= form_error('pesan', '<small class="form-text text-danger">', '</small>'); ?>
								</div>
								<div class="form-group text-center">
									<input type="submit" name="submit" value="Kirim" class="btn btn-primary" style="background-color: yellow; color: black;">
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<h3>Informasi Kontak</h3>
						<div class="row contact-info-wrap">
							<div class="col">
								<div class="row">
									<div class="col">
										<p><span><i class="icon-location"></i></span> Jalan Terusan Kopo KM 12 , Katapang Kab. Bandung 40921</p>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<p><span><i class="icon-phone3"></i></span> <a href="tel://62215436789">+ 62 21 543 6789</a></p>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<p><span><i class="icon-paperplane"></i></span> <a href="mailto:info@nekatshop.com">info@nekatshop.com</a></p>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<p><span><i class="icon-globe"></i></span> <a href="#">nekatshop.com</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>