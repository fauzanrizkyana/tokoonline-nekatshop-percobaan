<?= $this->session->flashdata('message'); ?>
<aside id="colorlib-hero">
	<div class="flexslider">
		<ul class="slides">
			<?php foreach ($banner as $bann) : ?>
				<li style="background-image: url(http://localhost/ci-adminlte/assets/img/<?= $bann['nama_file']; ?>);">
					<div class="overlay"></div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</aside>
<div class="colorlib-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
				<h2><span>Produk Baru</span></h2>
			</div>
		</div>
		<div class="row">
			<?php foreach ($produk_baru as $pb) : ?>
				<div class="col-md-3 text-center">
					<div class="product-entry">
						<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $pb['gambar_produk']; ?>);">
							<p class="tag">
								<span class="new">Baru</span>
								<?php if ($pb['diskon'] > 0) : ?>
									<span class="sale"><?= $pb['diskon']; ?>%</span>
								<?php endif; ?>
							</p>
							<div class="cart">
								<p>
									<?php if ($pb['stok_produk'] != 0) : ?>
										<span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $pb['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
									<?php endif; ?>
									<span><a href="<?= base_url(); ?>shop/detail/<?= $pb['id_produk']; ?>"><i class="icon-eye"></i></a></span>
									<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
										<?php $cek_wishlist = $this->db->get_where('t_wishlist', ['email' => $this->session->userdata('email'), 'id_produk' => $pb['id_produk']])->row_array();
												if ($cek_wishlist == null) : ?>
											<span><a href="<?= base_url(); ?>shop/addtowishlist/<?= $pb['id_produk']; ?>"><i class="icon-star"></i></a></span>
										<?php else : ?>
											<span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $cek_wishlist['id']; ?>"><i class="icon-star-full"></i></a></span>
										<?php endif; ?>
									<?php endif; ?>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="<?= base_url(); ?>shop/detail/<?= $pb['id_produk']; ?>"><?= $pb['nama_produk']; ?></a></h3>
							<?php if ($pb['diskon'] > 0) :
									$sale = $pb['harga_produk'] - ($pb['harga_produk'] * $pb['diskon'] / 100); ?>
								<p class="price"><span class="sale">Rp. <?= number_format($pb['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
							<?php else : ?>
								<p class="price"><span>Rp. <?= number_format($pb['harga_produk'], '0', ',', '.'); ?></span></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<div id="colorlib-intro" class="colorlib-intro" style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="intro-desc">
					<div class="text-salebox">
						<div class="text-lefts">
							<div class="sale-box">
								<div class="sale-box-top">
									<h2 class="number">100</h2>
									<span class="sup-1">%</span>
								</div>
								<h2 class="text-sale">Garansi</h2>
							</div>
						</div>
						<div class="text-rights">
							<h3 class="title">Garansi 100% untuk semua produk.</h3>
							<p>Barang yang tidak sesuai dengan harapan akan diganti hanya untukmu.</p>
							<small>*syarat dan ketentuan berlaku</small>
							<p><a href="#" class="btn btn-primary btn-outline">Selengkapnya</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="colorlib-shop">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
				<h2><span>Produk</span></h2>
			</div>
		</div>
		<div class="row">
			<?php foreach ($produk as $prdk) : ?>
				<div class="col-md-3 text-center">
					<div class="product-entry">
						<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $prdk['gambar_produk']; ?>);">
							<?php if ($prdk['diskon'] > 0) : ?>
								<p class="tag"><span class="sale"><?= $prdk['diskon']; ?>%</span></p>
							<?php endif; ?>
							<div class="cart">
								<p>
									<?php if ($prdk['stok_produk'] != 0) : ?>
										<span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $prdk['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
									<?php endif; ?>
									<span><a href="<?= base_url(); ?>shop/detail/<?= $prdk['id_produk']; ?>"><i class="icon-eye"></i></a></span>
									<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
										<?php $cek_wishlist = $this->db->get_where('t_wishlist', ['email' => $this->session->userdata('email'), 'id_produk' => $prdk['id_produk']])->row_array();
												if ($cek_wishlist == null) : ?>
											<span><a href="<?= base_url(); ?>shop/addtowishlist/<?= $prdk['id_produk']; ?>"><i class="icon-star"></i></a></span>
										<?php else : ?>
											<span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $cek_wishlist['id']; ?>"><i class="icon-star-full"></i></a></span>
										<?php endif; ?>
									<?php endif; ?>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="<?= base_url(); ?>shop/detail/<?= $prdk['id_produk']; ?>"><?= $prdk['nama_produk']; ?></a></h3>
							<?php if ($prdk['diskon'] > 0) :
									$sale = $prdk['harga_produk'] - ($prdk['harga_produk'] * $prdk['diskon'] / 100); ?>
								<p class="price"><span class="sale">Rp. <?= number_format($prdk['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
							<?php else : ?>
								<p class="price"><span>Rp. <?= number_format($prdk['harga_produk'], '0', ',', '.'); ?></span></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<div class="colorlib-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading">
				<h2>Berita Terkini</h2>
			</div>
		</div>
		<div class="row">
			<ul class="list-unstyled">
				<?php foreach ($berita as $brt) : ?>
					<li class="media my-4">
						<a href="<?= base_url(); ?>blog/detail/<?= $brt['id_berita']; ?>"><img src="http://localhost/ci-adminlte/assets/img3/<?= $brt['gambar']; ?>" class="mr-3" alt="..." width="300"></a>
						<div class="media-body">
							<h2 class="mt-0 mb-1"><a href="<?= base_url(); ?>blog/detail/<?= $brt['id_berita']; ?>"><?= $brt['judul']; ?></a></h2>
							<h5><?= date('d M Y H:i', $brt['tanggal']); ?> | <span>Di posting oleh:</span> <span><?= $brt['penulis']; ?></span></h5>
							<p><?= character_limiter($brt['isi_berita'], 150); ?></p>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>