        <div class="colorlib-shop">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
        				<h2><span>Hasil pencarian</span></h2>
        				<?php if (isset($kategori)) : ?>
        					<p>Kategori : <?= $kategori; ?> </p>
        				<?php endif; ?>
        			</div>
        		</div>
        		<div class="row">
        			<?php if ($produk == null) : ?>
        				<div class="col-md-12 text-center">
        					<h2><span>Tidak ditemukan barang yang sesuai</span></h2>
        				</div>
        			<?php else : ?>
        				<?php foreach ($produk as $p) : ?>
        					<div class="col-md-3 text-center">
        						<div class="product-entry">
        							<div class="product-img" style="background-image: url(http://localhost/ci-adminlte/assets/img2/<?= $p['gambar_produk']; ?>);">
        								<?php if ($p['diskon'] > 0) : ?>
        									<p class="tag"><span class="sale"><?= $p['diskon']; ?>%</span></p>
        								<?php endif; ?>
        								<div class="cart">
        									<p>
        										<?php if ($p['stok_produk'] != 0) : ?>
        											<span class="addtocart"><a href="<?= base_url(); ?>shop/tambahkeranjang/<?= $p['id_produk']; ?>"><i class="icon-shopping-cart"></i></a></span>
        										<?php endif; ?>
        										<span><a href="<?= base_url(); ?>shop/detail/<?= $p['id_produk']; ?>"><i class="icon-eye"></i></a></span>
        										<?php if ($this->session->userdata('username') != null && $this->session->userdata('email') != null && $this->session->userdata('status') != null) : ?>
        											<?php $cek_wishlist = $this->db->get_where('t_wishlist', ['email' => $this->session->userdata('email'), 'id_produk' => $p['id_produk']])->row_array();
																																		if ($cek_wishlist == null) : ?>
        												<span><a href="<?= base_url(); ?>shop/addtowishlist/<?= $p['id_produk']; ?>"><i class="icon-star"></i></a></span>
        											<?php else : ?>
        												<span><a href="<?= base_url(); ?>shop/deletewishlist/<?= $cek_wishlist['id']; ?>"><i class="icon-star-full"></i></a></span>
        											<?php endif; ?>
        										<?php endif; ?>
        									</p>
        								</div>
        							</div>
        							<div class="desc">
        								<h3><a href="<?= base_url(); ?>shop/detail/<?= $p['id_produk']; ?>"><?= $p['nama_produk']; ?></a></h3>
        								<?php if ($p['diskon'] > 0) :
																																		$sale = $p['harga_produk'] - ($p['harga_produk'] * $p['diskon'] / 100); ?>
        									<p class="price"><span class="sale">Rp. <?= number_format($p['harga_produk'], '0', ',', '.'); ?></span><br><span>Rp. <?= number_format($sale, '0', ',', '.'); ?></span></p>
        								<?php else : ?>
        									<p class="price"><span>Rp. <?= number_format($p['harga_produk'], '0', ',', '.'); ?></span></p>
        								<?php endif; ?>
        							</div>
        						</div>
        					</div>
        				<?php endforeach; ?>
        			<?php endif; ?>
        		</div>
        	</div>
        </div>