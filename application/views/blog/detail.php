        <div id="colorlib-about">
        	<div class="container">
        		<div class="row">
        			<div class="about-flex">
        				<div class="col-three-forth">
        					<h1><?= $berita->judul; ?></h1>
        					<p><?= date('d F Y H:i', $berita->tanggal); ?> | Diposting oleh <?= $berita->penulis; ?></p>
        					<div class="row">
        						<div class="col-md-12">
        							<div class="row row-pb-sm">
        								<div class="col-md-6">
        									<img class="float-left" src="http://localhost/ci-adminlte/assets/img3/<?= $berita->gambar; ?>" style="width: 700px;">
        								</div>
        								<?= $berita->isi_berita; ?>
        							</div>
        						</div>
        					</div>
        				</div>
        				<div class="col-one-forth">
        					<div class="row">
        						<div class="col-md-12 about">
        							<h2>Baca Juga</h2>
        							<ul>
        								<?php foreach ($recent_berita as $rb) : ?>
        									<li><a href="<?= base_url(); ?>blog/detail/<?= $rb['id_berita']; ?>"><?= $rb['judul']; ?></a></li>
        								<?php endforeach; ?>
        							</ul>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>