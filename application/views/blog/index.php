<aside id="colorlib-hero" class="breadcrumbs">
	<div class="flexslider">
		<ul class="slides">
			<li style="background-image: url(<?= base_url(); ?>assets/img/hero_2.jpg);">
				<div class="overlay"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
							<div class="slider-text-inner text-center">
								<h1><?= $judul; ?></h1>
								<h2 class="bread"><span><a href="<?= base_url(); ?>">Home</a></span> <span><?= $judul; ?></span></h2>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div class="colorlib-blog">
	<div class="container">
		<div class="row">
			<ul class="list-unstyled">
				<?php foreach ($berita as $brt) : ?>
					<li class="media my-4">
						<a href="<?= base_url(); ?>blog/detail/<?= $brt['id_berita']; ?>"><img src="http://localhost/ci-adminlte/assets/img3/<?= $brt['gambar']; ?>" class="mr-3" alt="..." width="300"></a>
						<div class="media-body">
							<h2 class="mt-0 mb-1"><a href="<?= base_url(); ?>blog/detail/<?= $brt['id_berita']; ?>"><?= $brt['judul']; ?></a></h2>
							<h5><?= date('d M Y H:i', $brt['tanggal']); ?> | <span>Di posting oleh:</span> <span><?= $brt['penulis']; ?></span></h5>
							<p><?= character_limiter($brt['isi_berita'], 150); ?></p>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>