<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Blog extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->blog_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        $data['judul'] = 'Berita';
        $data['user'] = $this->blog_model->getUser($this->session->userdata('email'));
        $data['berita'] = $this->blog_model->viewDaftarBerita();
        $this->load->view('templates/header', $data)->view('blog/index', $data)->view('templates/footer');
    }
    public function detail($id_berita)
    {
        $data['berita'] = $this->blog_model->getById($id_berita);
        $data['judul'] = $data['berita']->judul . ' - Berita';
        $data['user'] = $this->blog_model->getUser($this->session->userdata('email'));
        $data['recent_berita'] = $this->blog_model->viewRecentBerita();
        $this->load->view('templates/header', $data)->view('blog/detail', $data)->view('templates/footer');
    }
}
