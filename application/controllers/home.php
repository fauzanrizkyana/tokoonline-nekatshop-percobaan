<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->home_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        $data['judul'] = 'Home';
        $data['user'] = $this->home_model->getUser($this->session->userdata('email'));
        $data['produk_baru'] = $this->home_model->viewProdukBaru();
        $data['produk'] = $this->home_model->viewProduk();
        $data['berita'] = $this->home_model->viewBerita();
        $data['banner'] = $this->home_model->viewBanner();
        $this->load->view('templates/header', $data)->view('home/index', $data)->view('templates/footer');
    }
}
