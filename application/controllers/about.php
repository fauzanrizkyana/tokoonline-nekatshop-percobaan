<?php
defined('BASEPATH') or exit('No direct script access allowed');
class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->about_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        $data['judul'] = 'Tentang Kami';
        $data['user'] = $this->about_model->getUser($this->session->userdata('email'));
        $data['about'] = $this->about_model->getAbout();
        $this->load->view('templates/header', $data)->view('about/index', $data)->view('templates/footer');
    }
}
