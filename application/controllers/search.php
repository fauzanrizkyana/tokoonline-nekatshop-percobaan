<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Search extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->search_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        $data['judul'] = 'Pencarian';
        $data['user'] = $this->search_model->getUser($this->session->userdata('email'));
        $kategori = $this->input->post('jenis');
        if ($kategori == 'Semua' || $kategori == 'semua') {
            $data['produk'] = $this->search_model->cariProduk();
        } else {
            $judul = $this->search_model->getKategoriTitle($kategori);
            $data['kategori'] = $judul['jenis_produk'];
            $data['produk'] = $this->search_model->cariProdukKategori();
        }
        $this->load->view('templates/header', $data)->view('search/index', $data)->view('templates/footer');
    }
}
