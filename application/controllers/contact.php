<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contact extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->contact_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['judul'] = 'Kontak';
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('subjek', 'Subjek', 'required');
        $this->form_validation->set_rules('pesan', 'Pesan', 'required');
        if ($this->form_validation->run() == FALSE) {
            if ($this->session->userdata('email')) {
                $data['user'] = $this->contact_model->getUser($this->session->userdata('email'));
            } else {
                $data['user'] = null;
            }
            $this->load->view('templates/header', $data)->view('contact/index', $data)->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $this->contact_model->send();
                $this->session->set_flashdata('flash', 'Feedback berhasil dikirim');
                redirect('contact');
            }
        }
    }
}
