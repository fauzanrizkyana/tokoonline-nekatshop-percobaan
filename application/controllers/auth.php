<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Auth extends CI_Controller
{
    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Log in';
            $cookie = get_cookie('nekatshop');
            if ($this->session->userdata('status') == 'login') {
                redirect('home');
            } elseif ($cookie <> '') {
                $row = $this->auth_model->getByCookie($cookie);
                if ($row) {
                    $data_session = [
                        'username' => $row->username,
                        'email' => $row->email,
                        'status' => "login"
                    ];
                    $this->session->set_userdata($data_session);
                    redirect('home');
                } else {
                    $this->load->view('templates/auth_header', $data)->view('auth/index')->view('templates/auth_footer');
                }
            } else {
                $this->load->view('templates/auth_header', $data)->view('auth/index')->view('templates/auth_footer');
            }
        } else {
            $this->_login();
        }
    }
    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $remember = $this->input->post('remember');
        $user = $this->db->get_where('t_user', ['email' => $email])->row_array();
        if ($user != null) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    if ($remember) {
                        $key = random_string('alnum', 64);
                        set_cookie('nekatshop', $key, 3600 * 24);
                        $this->auth_model->updateCookie($key, $user['id_user']);
                    }
                    $data = [
                        'username' => $user['username'],
                        'email' => $user['email'],
                        'status' => 'login'
                    ];
                    $this->session->set_userdata($data);
                    redirect('home');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah</div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email ini belum di aktivasi</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email tidak registrasi</div>');
            redirect('auth');
        }
    }
    public function register()
    {
        $this->form_validation->set_rules('username', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.email]', [
            'is_unique' => 'Email ini sudah digunakan'
        ]);
        $this->form_validation->set_rules('no_telepon', 'No. Telepon', 'required|trim|numeric|is_unique[t_user.no_telepon]', [
            'is_unique' => 'No. telepon ini sudah digunakan'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
            'matches' => 'Password tidak cocok',
            'min_length' => 'Password terlalu pendek'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('accept', 'Accept', 'required', [
            'required' => 'Mohon centangkan field ini'
        ]);
        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Register';
            $cookie = get_cookie('nekatshop');
            if ($this->session->userdata('status') == 'login') {
                redirect('home');
            } elseif ($cookie <> '') {
                $row = $this->auth_model->getByCookie($cookie);
                if ($row) {
                    $data_session = [
                        'username' => $row->username,
                        'email' => $row->email,
                        'status' => "login"
                    ];
                    $this->session->set_userdata($data_session);
                    redirect('home');
                } else {
                    $this->load->view('templates/auth_header', $data)->view('auth/register')->view('templates/auth_footer');
                }
            } else {
                $this->load->view('templates/auth_header', $data)->view('auth/register')->view('templates/auth_footer');
            }
        } else {
            $this->auth_model->register();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun telah dibuat. Silakan login</div>');
            redirect('auth');
        }
    }
    public function logout()
    {
        delete_cookie('nekatshop');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('status');
        if ($this->cart->contents()) {
            $this->cart->destroy();
        }
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda telah logout. Terima kasih!</div>');
        redirect('home');
    }
}
