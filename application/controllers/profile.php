<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->home_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        if ($this->session->userdata('email') == null) {
            echo "<script>history.go(-1);</script>";
        } else {
            $data['judul'] = "Profil";
            $data['user'] = $this->profile_model->getUser($this->session->userdata('email'));
            $this->load->view('templates/header', $data)->view('profile/index', $data)->view('templates/footer');
        }
    }
    public function edit()
    {
        if ($this->session->userdata('email') == null) {
            echo "<script>history.go(-1);</script>";
        } else {
            $data['judul'] = "Edit Profil";
            $data['user'] = $this->profile_model->getUser($this->session->userdata('email'));
            $data['jenis_kelamin'] = $this->profile_model->getGender();
            $this->form_validation->set_rules('username', 'Username', 'required|trim');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required|trim');
            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data)->view('profile/edit', $data)->view('templates/footer');
            } else {
                $this->profile_model->ubahProfil($data['user']['image']);
                $data_session = [
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Profil berhasil diubah</div>');
                redirect('profile');
            }
        }
    }
    public function changeEmailPhone()
    {
        if ($this->session->userdata('email') == null) {
            echo "<script>history.go(-1);</script>";
        } else {
            $data['judul'] = "Ubah Email / No. Telepon";
            $data['user'] = $this->profile_model->getUser($this->session->userdata('email'));
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.email]', [
                'is_unique' => 'Email ini sudah digunakan'
            ]);
            $this->form_validation->set_rules('no_telepon', 'No. Telepon', 'required|trim|numeric|is_unique[t_user.no_telepon]', [
                'is_unique' => 'No. telepon ini sudah digunakan'
            ]);
            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data)->view('profile/changeemailphone', $data)->view('templates/footer');
            } else {
                $id = $data['user']['id_user'];
                $nama = $data['user']['username'];
                $email = $this->input->post('email');
                $no_telepon = $this->input->post('no_telepon');
                $this->profile_model->gantiEmailNoTelepon($id, $email, $no_telepon);

                $data_session = [
                    'username' => $nama,
                    'email' => $email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Email atau No. telepon berhasil diubah</div>');
                redirect('profile');
            }
        }
    }
    public function changePassword()
    {
        if ($this->session->userdata('email') == null) {
            echo "<script>history.go(-1);</script>";
        } else {
            $data['judul'] = "Ubah Password";
            $data['user'] = $this->profile_model->getUser($this->session->userdata('email'));
            $this->form_validation->set_rules('current_password', 'Password Saat Ini', 'required|trim');
            $this->form_validation->set_rules('new_password1', 'Password Baru', 'required|trim|min_length[3]|matches[new_password2]', [
                'matches' => "Password tidak cocok",
                'min_length' => "Password terlalu pendek"
            ]);
            $this->form_validation->set_rules('new_password2', 'Konfirmasi Password Baru', 'required|trim|min_length[3]|matches[new_password1]', [
                'matches' => "Password tidak cocok",
                'min_length' => "Password terlalu pendek"
            ]);
            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data)->view('profile/changepassword', $data)->view('templates/footer');
            } else {
                $current_password = $this->input->post('current_password');
                $new_password = $this->input->post('new_password1');
                if (!password_verify($current_password, $data['user']['password'])) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password saat ini salah</div>');
                    redirect('profile/changepassword');
                } else {
                    if ($current_password == $new_password) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password baru tidak boleh sama dengan password saat ini</div>');
                        redirect('profile/changepassword');
                    } else {
                        $this->profile_model->gantiPassword($new_password, $data['user']['email']);
                        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password anda telah diubah</div>');
                        redirect('profile');
                    }
                }
            }
        }
    }
}
