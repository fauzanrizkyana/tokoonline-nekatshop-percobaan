<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Shop extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cookie = get_cookie('nekatshop');
        if ($cookie <> '') {
            $row = $this->shop_model->getByCookie($cookie);
            if ($row) {
                $data_session = [
                    'username' => $row->username,
                    'email' => $row->email,
                    'status' => "login"
                ];
                $this->session->set_userdata($data_session);
            }
        }
    }
    public function index()
    {
        $data['judul'] = 'Shop';
        $kategori = $this->input->get('category');
        $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
        $config['base_url'] = base_url() . 'shop/index';
        if ($kategori) {
            $config['total_rows'] = $this->shop_model->hitungProdukBerdasarkanKategori($kategori);
        } else {
            $config['total_rows'] = $this->shop_model->hitungProduk();
        }
        $config['per_page'] = 12;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['start'] = $this->uri->segment(3);
        if ($kategori) {
            $data['produk'] = $this->shop_model->viewProdukBerdasarkanKategori($config['per_page'], $data['start'], $kategori);
        } else {
            $data['produk'] = $this->shop_model->viewProduk($config['per_page'], $data['start']);
        }
        $this->load->view('templates/header', $data)->view('shop/index', $data)->view('templates/footer');
    }
    public function detail($id_produk)
    {
        $data['produk'] = $this->shop_model->getById($id_produk);
        $active = $data['produk']['active'];
        if ($active == 0) {
            echo "<script>history.go(-1);</script>";
        } else {
            $data['judul'] = $data['produk']['nama_produk'];
            $data['judul2'] = 'Detail Produk';
            $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
            $data['recent_produk'] = $this->shop_model->viewRecentProduk();
            $this->load->view('templates/header', $data)->view('shop/detail', $data)->view('templates/footer');
        }
    }
    public function tambahKeranjang($id_produk)
    {
        $data['produk'] = $this->shop_model->getById($id_produk);
        $active = $data['produk']['active'];
        $stok = $data['produk']['stok_produk'];
        if ($active == 0) {
            echo "<script>history.go(-1);</script>";
        } else {
            if ($stok > 0) {
                $data['judul'] = $data['produk']['nama_produk'];
                $data['judul2'] = 'Detail Produk';
                $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
                $this->load->view('templates/header', $data)->view('shop/tambahkeranjang', $data)->view('templates/footer');
            } else {
                echo "<script>history.go(-1);</script>";
            }
        }
    }
    public function tambah()
    {
        $produk = $this->shop_model->getById($this->input->post('id'));
        if ($this->input->post('qty') > $produk['stok_produk']) {
            echo "<script>
          history.go(-1);
          </script>";
        } else {
            $data_produk = [
                'id' => $this->input->post('id'),
                'name' => $this->input->post('nama'),
                'price' => $this->input->post('harga'),
                'gambar' => $this->input->post('gambar'),
                'ukuran' => $this->input->post('ukuran'),
                'qty' => $this->input->post('qty')
            ];
            $this->cart->insert($data_produk);
            echo "<script>history.go(-2);</script>";
        }
    }
    public function addtowishlist($id)
    {
        $user = $this->shop_model->getUser($this->session->userdata('email'));
        $this->shop_model->tambahWishlist($id, $user['email']);
        echo "<script>history.go(-1);</script>";
    }
    public function wishlist()
    {
        if ($this->session->userdata('status') != "login") {
            echo "<script>history.go(-1);</script>";
        }
        $data['judul'] = 'Wishlist';
        $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
        $data['produk'] = $this->shop_model->getWishlist($data['user']['email']);
        $this->load->view('templates/header', $data)->view('shop/wishlist', $data)->view('templates/footer');
    }
    public function deleteWishlist($id)
    {
        $this->shop_model->hapusWishlist($id);
        echo "<script>history.go(-1);</script>";
    }
    public function cart()
    {
        $data['judul'] = 'Keranjang';
        $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
        $data['cart'] = $this->cart->contents();
        $this->load->view('templates/header', $data)->view('shop/cart')->view('templates/footer');
    }
    public function hapus($rowid)
    {
        if ($rowid == "all") {
            $this->cart->destroy();
        } else {
            $data = [
                'rowid' => $rowid,
                'qty' => 0
            ];
            $this->cart->update($data);
        }
        redirect('shop/cart');
    }
    public function ubah()
    {
        $cart_info = $_POST['cart'];
        foreach ($cart_info as $id => $cart) {
            $rowid = $cart['rowid'];
            $price = $cart['price'];
            $gambar = $cart['gambar'];
            $amount = $price * $cart['qty'];
            $qty = $cart['qty'];
            $data = [
                'rowid' => $rowid,
                'price' => $price,
                'gambar' => $gambar,
                'amount' => $amount,
                'qty' => $qty
            ];
            $this->cart->update($data);
        }
        redirect('shop/cart');
    }
    public function checkout()
    {
        if ($this->cart->contents() == null) {
            echo "<script>history.go(-1);</script>";
        }
        $data['judul'] = 'Checkout';
        if ($this->session->userdata('email')) {
            $data['user'] = $this->shop_model->getUser($this->session->userdata('email'));
        } else {
            redirect('shop/auth');
        }
        $data['total'] = $this->cart->total();
        $data['produk'] = $this->cart->contents();
        $data['kodePembayaran'] = $this->shop_model->getKodePembayaran();
        $this->load->view('templates/header', $data)->view('shop/checkout', $data)->view('templates/footer');
    }
    public function auth()
    {
        if ($this->input->post('login')) {
            $this->form_validation->set_rules('email_login', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password_login', 'Password', 'trim|required');
        } elseif ($this->input->post('daftar')) {
            $this->form_validation->set_rules('username', 'Nama', 'required|trim');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required|trim');
            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.email]', [
                'is_unique' => 'Email ini sudah digunakan'
            ]);
            $this->form_validation->set_rules('no_telepon', 'Email', 'required|trim|numeric|is_unique[t_user.no_telepon]', [
                'is_unique' => 'No. telepon ini sudah digunakan'
            ]);
            $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
                'matches' => 'Password tidak cocok',
                'min_length' => 'Password terlalu pendek'
            ]);
            $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
            $this->form_validation->set_rules('accept', 'Accept', 'required', [
                'required' => 'Mohon centangkan field ini'
            ]);
        }
        if ($this->form_validation->run() == false) {
            if ($this->session->userdata('status') == 'login') {
                echo "<script>history.go(-1);</script>";
            } elseif ($this->cart->contents() == null) {
                echo "<script>history.go(-1);</script>";
            } else {
                $data['judul'] = 'Checkout';
                $this->load->view('templates/header', $data)->view('shop/auth', $data)->view('templates/footer');
            }
        } else {
            if ($this->input->post('login')) {
                $this->_login();
            } else {
                $this->auth_model->register();
                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $data = [
                    'username' => $username,
                    'email' => $email,
                    'status' => 'login'
                ];
                $this->session->set_userdata($data);
                redirect('shop/checkout');
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun telah dibuat. Silakan login</div>');
                redirect('shop/checkout');
            }
        }
    }
    private function _login()
    {
        $email = $this->input->post('email_login');
        $password = $this->input->post('password_login');
        $remember = $this->input->post('remember');
        $user = $this->db->get_where('t_user', ['email' => $email])->row_array();
        if ($user != null) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    if ($remember) {
                        $key = random_string('alnum', 64);
                        set_cookie('nekatshop', $key, 3600 * 24);
                        $this->auth_model->updateCookie($key, $user['id']);
                    }
                    $data = [
                        'username' => $user['username'],
                        'email' => $user['email'],
                        'status' => 'login'
                    ];
                    $this->session->set_userdata($data);
                    redirect('shop/checkout');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah</div>');
                    redirect('shop/auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email ini belum di aktivasi</div>');
                redirect('shop/auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email tidak registrasi</div>');
            redirect('shop/auth');
        }
    }
    function rajaongkir_get_provinsi()
    {
        $provinsi = $this->rajaongkir->province();
        $obj = json_decode($provinsi, true);
        $select_prov = "<option>- Pilih Provinsi -</option>";
        if ($obj['rajaongkir']['status']['code'] == 200) {
            for ($i = 0; $i < count($obj['rajaongkir']['results']); $i++) {
                $select_prov .= "<option value='" . $obj['rajaongkir']['results'][$i]['province_id'] . "'>" . $obj['rajaongkir']['results'][$i]['province'] . "</option>";
            }
        } else {
            $select_prov .= "<option>- Pilih Provinsi -</option>";
        }
        echo $select_prov;
    }
    function rajaongkir_get_kota()
    {
        $id_province = $this->input->post('id_province', TRUE);
        $kota = $this->rajaongkir->city($id_province);
        $obj = json_decode($kota, true);
        $select_kotkab = "<option>- Pilih Kota / Kabupaten -</option>";
        if ($obj['rajaongkir']['status']['code'] == 200) {
            for ($i = 0; $i < count($obj['rajaongkir']['results']); $i++) {
                $select_kotkab .= "<option value='" . $obj['rajaongkir']['results'][$i]['city_id'] . "'>" . $obj['rajaongkir']['results'][$i]['city_name'] . "</option>";
            }
        } else {
            $select_kotkab .= "<option>- Pilih Kota / Kabupaten -</option>";
        }
        echo $select_kotkab;
    }
    function rajaongkir_get_kode_pos()
    {
        $id_kota = $this->input->post('id_kota', true);
        $id_province = $this->input->post('id_province', true);
        if ($id_kota == 0 || $id_province == 0) {
            $postal = '<input type="text" id="kode_pos" class="form-control form-control-sm" style="height: 36px;" placeholder="Kode Pos">';
        } else {
            $kode_pos = $this->rajaongkir->city($id_province, $id_kota);
            $obj = json_decode($kode_pos, true);
            $postal = '<input type="text" id="kode_pos" class="form-control form-control-sm" style="height: 36px;" placeholder="Kode Pos" ';
            if ($obj['rajaongkir']['status']['code'] == 200) {
                $postal .= 'value="' .  $obj['rajaongkir']['results']['postal_code'] . '">';
            } else {
                $postal .= 'value="">';
            }
        }
        echo $postal;
    }
    function rajaongkir_get_cost()
    {
        $kota_asal      = 22;
        $kota_tujuan    = $this->input->post('kota_tujuan', TRUE);
        $berat          = $this->input->post('berat', TRUE);
        $kurir          = $this->input->post('kurir_pengiriman', TRUE);
        $jenis = $this->rajaongkir->cost($kota_asal, $kota_tujuan, $berat, $kurir);
        $obj = json_decode($jenis, true);
        $datas = '<option value="0">- Pilih Paket -</option>';
        if ($obj['rajaongkir']['status']['code'] == 200) {
            for ($i = 0; $i < count($obj['rajaongkir']['results']); $i++) {
                for ($j = 0; $j < count($obj['rajaongkir']['results'][$i]['costs']); $j++) {
                    $nama_pengiriman = $obj['rajaongkir']['results'][$i]['name'];
                    $service         = $obj['rajaongkir']['results'][$i]['costs'][$j]['service'];
                    $biaya           = $obj['rajaongkir']['results'][$i]['costs'][$j]['cost'][0]['value'];
                    $biaya_format    = number_format($obj['rajaongkir']['results'][$i]['costs'][$j]['cost'][0]['value'], '0', ',', '.');
                    $lama_pengiriman = $obj['rajaongkir']['results'][$i]['costs'][$j]['cost'][0]['etd'];
                    $datas .= '<option value="' . $biaya . '">' . $nama_pengiriman . ' - ' . $service . ' Rp. ' . $biaya_format . ' / Pengiriman : ' . $lama_pengiriman . ' hari</option>';
                }
            }
        } else {
            $datas .= '<option value="0">- Pilih Paket -</option>';
        }
        echo $datas;
    }
    function rajaongkir_get_paket()
    {
        $harga          = $this->input->post('kurir_div', TRUE);
        $kota_asal      = 22;
        $kota_tujuan    = $this->input->post('kota_tujuan', TRUE);
        $berat          = $this->input->post('berat', TRUE);
        $kurir          = $this->input->post('kurir_pengiriman', TRUE);
        if ($harga == 0) {
            $datas = "<input type='hidden' name='nama_paket' value=''>
            <input type='hidden' name='jenis_paket' value=''>
            <input type='hidden' name='lama_pengiriman' value=''>";
        } else {
            $jenis = $this->rajaongkir->cost($kota_asal, $kota_tujuan, $berat, $kurir);
            $obj = json_decode($jenis, true);
            $datas = "";
            if ($obj['rajaongkir']['status']['code'] == 200) {
                for ($i = 0; $i < count($obj['rajaongkir']['results']); $i++) {
                    for ($j = 0; $j < count($obj['rajaongkir']['results'][$i]['costs']); $j++) {
                        if ($obj['rajaongkir']['results'][$i]['costs'][$j]['cost'][0]['value'] == $harga) {
                            $nama_pengiriman = $obj['rajaongkir']['results'][$i]['name'];
                            $service         = $obj['rajaongkir']['results'][$i]['costs'][$j]['service'];
                            $lama_pengiriman = $obj['rajaongkir']['results'][$i]['costs'][$j]['cost'][0]['etd'];
                            $datas .= "<input type='hidden' name='nama_paket' value='" . $nama_pengiriman . "'>
                                <input type='hidden' name='jenis_paket' value='" . $service . "'>
                                <input type='hidden' name='lama_pengiriman' value='" . $lama_pengiriman . "'>";
                        } else {
                            $datas .= "";
                        }
                    }
                }
            } else {
                $datas .= "<input type='hidden' name='nama_paket' value=''>
                    <input type='hidden' name='jenis_paket' value=''>
                    <input type='hidden' name='lama_pengiriman' value=''>";
            }
        }
        echo $datas;
    }
}
