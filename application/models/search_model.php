<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Search_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function cariProduk()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nama_produk', $keyword);
        return $this->db->get('t_produk')->result_array();
    }
    public function getKategoriTitle($kategori)
    {
        return $this->db->get_where('t_kategori', ['id' => $kategori])->row_array();
    }
    public function cariProdukKategori()
    {
        $keyword = $this->input->post('keyword', true);
        $jenis = $this->input->post('jenis');
        $this->db->like('nama_produk', $keyword);
        $this->db->like('id_jenis_produk', $jenis);
        return $this->db->get('t_produk')->result_array();
    }
}
