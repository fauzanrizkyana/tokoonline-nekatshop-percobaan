<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Profile_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->select('*')
            ->from('t_user')
            ->join('t_jenis_kelamin', 't_user.id_jenis_kelamin = t_jenis_kelamin.id')
            ->where(['t_user.email' => $email])
            ->get()->row_array();
    }
    public function getGender()
    {
        return $this->db->get('t_jenis_kelamin')->result_array();
    }
    public function ubahProfil($gambar_lama)
    {
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $jenis_kelamin = $this->input->post('jenis_kelamin', true);
        $tanggal = $this->input->post('tanggal_lahir', true);
        list($month, $day, $year) = explode('/', $tanggal);
        $date = mktime(null, null, null, $month, $day, $year);

        $upload_image = $_FILES['image']['name'];
        if ($upload_image) {
            $config['upload_path'] = './assets/img2/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['remove_space'] = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $old_image = $gambar_lama;
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'assets/img2/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('username', $username);
        $this->db->set('tanggal_lahir', $date);
        $this->db->set('id_jenis_kelamin', $jenis_kelamin);
        $this->db->where('email', $email);
        $this->db->update('t_user');
    }
    public function gantiEmailNoTelepon($id, $email, $no_telepon)
    {
        $this->db->where('id_user', $id)->update('t_user', ['email' => $email, 'no_telepon' => $no_telepon]);
    }
    public function gantiPassword($new_password, $email)
    {
        $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
        $this->db->set('password', $password_hash);
        $this->db->where('email', $email);
        $this->db->update('t_user');
    }
}
