<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Blog_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function viewDaftarBerita()
    {
        $this->db->order_by('id_berita', 'desc');
        return $this->db->get('t_berita')->result_array();
    }
    public function getById($id_berita)
    {
        return $this->db->get_where('t_berita', ['id_berita' => $id_berita])->row();
    }
    public function viewRecentBerita()
    {
        $this->db->order_by('id_berita', 'random');
        $this->db->order_by('judul', 'random');
        $this->db->order_by('tanggal', 'random');
        return $this->db->get('t_berita', 3)->result_array();
    }
}
