<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Shop_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function viewProduk($limit, $start)
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_kategori.active' => 1])
            ->order_by('t_produk.id_produk', 'asc')
            ->limit($limit, $start)
            ->get()->result_array();
    }
    public function viewProdukBerdasarkanKategori($limit, $start, $id_jenis_produk)
    {
        return $this->db->get_where('t_produk', ['id_jenis_produk' => $id_jenis_produk], $limit, $start)->result_array();
    }
    public function hitungProduk()
    {
        return $this->db->get('t_produk')->num_rows();
    }
    public function hitungProdukBerdasarkanKategori($id_jenis_produk)
    {
        return $this->db->get_where('t_produk', ['id_jenis_produk' => $id_jenis_produk])->num_rows();
    }
    public function getById($id_produk)
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_produk.id_produk' => $id_produk, 't_kategori.active' => 1])
            ->get()->row_array();
    }
    public function viewRecentProduk()
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_kategori.active' => 1])
            ->order_by('id_produk', 'random')
            ->order_by('nama_produk', 'random')
            ->order_by('deskripsi_produk', 'random')
            ->order_by('harga_produk', 'random')
            ->limit(4)
            ->get()->result_array();
    }
    public function tambahWishlist($id, $email)
    {
        $data = [
            'email' => $email,
            'id_produk' => $id
        ];
        $this->db->insert('t_wishlist', $data);
    }
    public function getWishlist($email)
    {
        return $this->db->select('*')
            ->from('t_wishlist')
            ->join('t_produk', 't_wishlist.id_produk = t_produk.id_produk')
            ->where(['t_wishlist.email' => $email])
            ->get()->result_array();
    }
    public function hapusWishlist($id)
    {
        $this->db->delete('t_wishlist', ['id' => $id]);
    }
    public function getKodePembayaran()
    {
        $tahun = date('Y', time());
        $bulan = date('m', time());
        $hari = date('d', time());
        $kodepem = $tahun . $bulan . $hari;
        $kode = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        for ($i = 0; $i < 16; $i++) {
            $rand = random_element($kode);
            $kodepem = $kodepem . $rand;
        }
        $kodepem = strtoupper($kodepem);
        return $kodepem;
    }
}
