<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Auth_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function updateCookie($key, $id)
    {
        $data = ["cookie" => $key];
        $this->db->where('id_user', $id)->update('t_user', $data);
    }
    public function register()
    {
        $tanggal = $this->input->post('tanggal_lahir', true);
        list($month, $day, $year) = explode('/', $tanggal);
        $date = mktime(null, null, null, $month, $day, $year);
        $data = [
            'username' => htmlspecialchars($this->input->post('username', true)),
            'tanggal_lahir' => $date,
            'id_jenis_kelamin' => htmlspecialchars($this->input->post('jenis_kelamin', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'no_telepon' => htmlspecialchars($this->input->post('no_telepon', true)),
            'image' => 'default.jpg',
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'is_active' => 1,
            'date_created' => time(),
            'cookie' => null
        ];
        $this->db->insert('t_user', $data);
    }
}
