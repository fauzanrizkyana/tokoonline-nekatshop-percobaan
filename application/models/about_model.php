<?php
defined('BASEPATH') or exit('No direct script access allowed');
class About_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function getAbout()
    {
        return $this->db->get('t_about')->result();
    }
}
