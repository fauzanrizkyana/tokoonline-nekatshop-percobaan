<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home_Model extends CI_Model
{
    public function viewBerita()
    {
        $this->db->order_by('id_berita', 'desc');
        return $this->db->get('t_berita', 3)->result_array();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function viewProduk()
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_kategori.active' => 1])
            ->limit(8)
            ->get()->result_array();
    }
    public function viewProdukBaru()
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_kategori.active' => 1])
            ->order_by('t_produk.id_produk', 'desc')
            ->limit(4)
            ->get()->result_array();
    }
    public function viewBanner()
    {
        return $this->db->get('t_banner')->result_array();
    }
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
}
