<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contact_Model extends CI_Model
{
    public function getByCookie($cookie)
    {
        return $this->db->get_where('t_user', ["cookie" => $cookie])->row();
    }
    public function getUser($email)
    {
        return $this->db->get_where('t_user', ['email' => $email])->row_array();
    }
    public function send()
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = [
            "nama" => $this->input->post('nama', true),
            "email" => $this->input->post('email', true),
            "tanggal" => time(),
            "subjek" => $this->input->post('subjek', true),
            "pesan" => $this->input->post('pesan', true)
        ];

        $this->db->insert('t_feedback', $data);
    }
}
