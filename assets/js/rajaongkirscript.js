//* select Provinsi */
var base_url = "http://localhost/nekatshop/";
$.ajax({
    type: 'post',
    url: base_url + 'shop/rajaongkir_get_provinsi',
    data: {},
    dataType: 'html',
    success: function (data) {
        $("#list_provinsi").html(data);
    },
    beforeSend: function () {

    },
    complete: function () {

    }
});
/* select Provinsi */

$("#list_provinsi").change(function () {
    var id_province = this.value;
    kota(id_province);
    $("#div_kota").show();
});

/* select Kota */
kota = function (id_province) {
    $.ajax({
        type: 'post',
        url: base_url + 'shop/rajaongkir_get_kota',
        data: { id_province: id_province },
        dataType: 'html',
        success: function (data) {
            $("#list_kotakab").html(data);
        }
    });

}

$("#list_kotakab").change(function () {
    var id_kota = this.value;
    var id_province = $("#list_provinsi").val();
    postal(id_kota, id_province);
    $("#div_kodepos").show();
})

postal = function (id_kota, id_province) {
    $.ajax({
        type: 'post',
        url: base_url + 'shop/rajaongkir_get_kode_pos',
        data: { id_kota: id_kota, id_province: id_province },
        dataType: 'html',
        success: function (data) {
            $("#kode_pos").html(data);
        }
    });
}


$("#list_kurir").change(function () {
    var id_kurir = this.value;
    var id_kota = $("#list_kotakab").val();
    var qty = $("#berat_total").val();
    cost(id_kurir, id_kota, qty);
    $("#div_kurir").show();
});


cost = function (id_kurir, id_kota, qty) {
    $.ajax({
        type: 'post',
        url: base_url + 'shop/rajaongkir_get_cost',
        data: { kurir_pengiriman: id_kurir, kota_tujuan: id_kota, berat: qty },
        dataType: 'html',
        success: function (data) {
            $("#list_kurir_div").html(data);
        }
    });

}

$("#list_kurir_div").change(function () {
    var id_kurir_div = this.value;
    var kurir = $("#list_kurir").val();
    var kota = $("#list_kotakab").val();
    var qty = $("#berat_total").val();
    paket(id_kurir_div, kurir, kota, qty);
});

paket = function (id_kurir_div, kurir, kota, qty) {
    $.ajax({
        type: 'post',
        url: base_url + 'shop/rajaongkir_get_paket',
        data: { kurir_div: id_kurir_div, kurir_pengiriman: kurir, kota_tujuan: kota, berat: qty },
        dataType: 'html',
        success: function (data) {
            $("#jenis_paket").html(data);
        }
    });
}

